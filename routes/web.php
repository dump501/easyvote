<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'FrontViewController@welcome');

Route::get('/send_sms', 'smsController@sendBird');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm')->name('adminLogin');
Route::get('/login/candidat', 'Auth\LoginController@showCandidatLoginForm')->name('candidatLogin');
Route::get('/login/votant', 'Auth\LoginController@showVotantLoginForm')->name('votantLogin');
Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm')->name('adminRegister');
Route::get('/register/candidat', 'Auth\RegisterController@showCandidatRegisterForm')->name('candidatRegister');
Route::get('/register/votant', 'Auth\RegisterController@showVotantRegisterForm')->name('votantRegister');

Route::post('/login/admin', 'Auth\LoginController@adminLogin');
Route::post('/login/candidat', 'Auth\LoginController@candidatLogin');
Route::post('/login/votant', 'Auth\LoginController@votantLogin');
Route::post('/register/admin', 'Auth\RegisterController@createAdmin');
Route::post('/register/candidat', 'Auth\RegisterController@createCandidat');
Route::post('/register/votant', 'Auth\RegisterController@createVotant');

/*Route::get('/admin', 'AdminController@home')->name('admin')->middleware('admin');
Route::view('/votant', 'VotantController@home')->name('votant');
Route::view('/candidat', 'CandidatController@home')->name('candidat');*/



/**
 * All admin routes
 */

Route::group(['prefix'=>'admin','namespace'=>'Admin','middleware'=>['admin'],'as'=>'admin.'], function(){
    //All the admin routes will be defined here...
    Route::get('/', 'AdminController@home')->name('home');
    Route::resource('candidats', 'CandidatController');
    Route::resource('elections', 'ElectionController');
    Route::resource('votes', 'VoteController');
    Route::resource('parties', 'PartyController');
    Route::resource('votants', 'VotantController');
  });


/**
 * All votant routes
 */

Route::group(['prefix'=>'votant','namespace'=>'Votant','middleware'=>['votant'],'as'=>'votant.'], function(){
    //All the votant routes will be defined here...
    Route::get('/', 'VotantController@home')->name('home');
    Route::resource('votes', 'VoteController');
    Route::resource('elections', 'ElectionController');
    Route::resource('parties', 'PartyController');
    Route::post('votes/find_election_party', 'VoteController@findElectionParty')->name('votes.findElectionParty');
  });

 /**
 * All candidat routes
 */

Route::group(['prefix'=>'candidat','namespace'=>'Candidat','middleware'=>['candidat'],'as'=>'candidat.'], function(){
    //All the candidat routes will be defined here...
    Route::get('/', 'CandidatController@home')->name('home');
    Route::resource('votes', 'VoteController');
    Route::resource('parties', 'PartyController');
    Route::resource('elections', 'ElectionController');
  });


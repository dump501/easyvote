@extends('layouts.admin')

@section('content')
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="card-header">
                        <center>
                            <h3>Create Party</h3>
                        </center>
                    </div>

                    <form action="{{ route('admin.parties.store') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-12 p-t-10">
                                <label for="name">Name of party</label>
                            </div>
                            <div class="col-12">
                                <input type="text" name="name" id="name" class="form-control">
                            </div><br/><br/>
                            <div class="col-12 p-t-10">
                                <label for="name">Description of party</label>
                            </div>
                            <div class="col-12">
                                <input type="text" name="description" id="name" class="form-control">
                            </div><br/><br/>
                            <div class="col-12">
                                <label for="selectLg" class=" form-control-label">Select The election that the party will be enrolled</label>
                            </div>
                            <div class="col-12">
                                <select name="election_id" id="select" class="form-control">
                                    <option value="0">Please select</option>
                                    @foreach ($elections as $election)
                                    <option value="{{ $election->id }}">{{ $election->name }}</option>
                                    @endforeach
                                </select>
                            </div><br><br>
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

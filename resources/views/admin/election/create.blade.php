@extends('layouts.admin')

@section('content')
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="card-header">
                        <center>
                            <h3>Create Election</h3>
                        </center>
                    </div>

                    <form action="{{ route('admin.elections.store') }}" method="post">
                        <div class="row">
                            @csrf
                            <div class="col-12 p-t-10">
                                <label for="name">Name of election</label>
                            </div>
                            <div class="col-12">
                                <input type="text" name="name" id="name" class="form-control">
                            </div><br><br>
                            <div class="col-12 p-t-10">
                                <label for="description">Description of election</label>
                            </div>
                            <div class="col-12">
                                <textarea name="description" id="description" class="form-control" rows="8"></textarea>
                            </div><br><br>
                            <div class="col-12 p-t-10">
                                <label for="done_at">date that the election will take place</label>
                            </div>
                            <div class="col-12">
                                <input type="datetime-local" name="done_at" id="" class="form-control">
                            </div>
                            <div class="col-12 p-t-10">
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.admin')
@section('content')
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row m-t-30">
            <?php if($election = null): ?>
                <div class="col-md-12">
                    <H1>No election today</H1>
                </div>
            <?php endif?>
                <?php if ($voteArray == []): ?>
                <div class="col-md-12">
                    <H1>NO vote yet</H1>
                </div>
                <?php else :?>
                <div class="col-md-12">
                <h1>Elections results and statistic</h1>
                    <h1>The Winner of the Election is : {{ $winner }}</h1><br><br>
                    <!-- DATA TABLE-->
                    <div class="table-responsive m-b-40">
                        <table class="table table-borderless table-data3">
                            <thead>
                            <tr>
                                <th>Party</th>
                                <th>Voices</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($voteArray as $item)
                                <tr>
                                    <td>{{ $item['party'] }}</td>
                                    <td>{{ $item['vote']}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- END DATA TABLE-->
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
@endsection

@endsection

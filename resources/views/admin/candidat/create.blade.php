@extends('layouts.admin')

@section('content')
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="card-header">
                        <center>
                            <h3>Add candidat</h3>
                        </center>
                    </div>

                    <form action="{{ route('admin.candidats.store') }}" method="post">
                        <div class="row">
                            @csrf
                            <div class="col-12 p-t-10">
                                <label for="name">Name</label>
                            </div>
                            <div class="col-12">
                                <input type="text" name="name" id="name" class="form-control">
                            </div><br><br>
                            <div class="col-12 p-t-10">
                                <label for="email">email</label>
                            </div>
                            <div class="col-12">
                                <input type="text" name="email" id="email" class="form-control">
                            </div><br><br>
                            <div class="col-12 p-t-10">
                                <label for="phone">phone number</label>
                            </div>
                            <div class="col-12">
                                <input type="number" name="phone" id="" class="form-control">
                            </div><br><br>
                            <div class="col-12 p-t-10">
                                <label for="phone">Password</label>
                            </div>
                            <div class="col-12">
                                <input type="text" name="password" id="" class="form-control">
                            </div><br><br>
                            <div class="col-12 p-t-10">
                                <label for="phone">Select Party</label>
                            </div>
                            <select name="party_id" id="select" class="form-control">
                                <option value="0">Please select</option>
                                @foreach ($parties as $party)
                                    <option value="{{ $party->id }}">{{ $party->name }}</option>
                                @endforeach
                            </select>
                            <div class="col-12 p-t-10">
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

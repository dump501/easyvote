@extends('layouts.votant')

@section('content')
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="card">
                <form action="{{ route('votant.votes.store') }}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="card-header">
                            <center><h3>Votes</h3></center>
                        </div>

                        <?php if(!isset($parties)) { ?>
                            <div class="row">
                                <div class="col-12">
                                    <h1>No election Today</h1>
                                </div>
                            </div>
                        <?php } else {?>

                        <?php if(!isset($vote[0])): ?>


                        <div class="row">
                            <div class="col-12 p-t-10">
                                <select name="party" id="parties" class="form-control">
                                    <option value="0">Select The Party you want to vote</option>
                                    @foreach($parties as $party)
                                        <option value="{{ $party->id }}">{{ $party->name }}</option>
                                    @endforeach
                                </select>
                            </div><br><br>
                            <div class="col-12 p-t-10">
                                <button type="submit" class="btn btn-primary">Vote</button>
                            </div>
                        </div>

                        <?php else: ?>
                            <div>
                                <h1>You have already vote</h1>
                            </div>
                        <?php endif?>
                    </div>
                </form>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <!-- DATA TABLE-->
                    <div class="table-responsive m-b-40">
                    <h3>Election results and statistic</h3><br>
                        <table class="table table-borderless table-data3">
                            <thead>
                                <tr>
                                    <th>party name</th>
                                    <th>number of votes</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($voteArray as $voteItem)
                                <tr>
                                    <td>{{ $voteItem['party'] }}</td>
                                    <td>{{ $voteItem['vote']}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- END DATA TABLE-->
                </div>
                </div>
            </div>
        </div>

        <?php } ?>
    </div>
@endsection

@section('script')
<script type="text/javascript">
    /*$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });*/

    $(document).ready(function () {

        console.log('ready');

        $('#election').on('change',function(e) {

            console.log('sd');

         var election_id = e.target.value;

         $.ajax({

               url:"{{ route('votant.votes.findElectionParty') }}",
               type:"POST",
               data: {
                "_token": "{{ csrf_token() }}",
                "election_id": election_id,
                },

               success:function (data) {
                   console.log(data);
                $('#parties').empty();

                $.each(data.parties,function(index,party){

                    $('#parties').append('<option value="'+party.id+'">'+party.name+'</option>');
                });

               },
           })
        })

    });
</script>
@endsection

@extends('layouts/master')

@section('navbar_horizontal')
    <?php if(isset($electionDate[0])): ?>
    <input type="hidden" name="serverDate" value="{{ $electionDate[0] }}">
    <input type="hidden" name="launchDate" value="{{ $electionDate[1] }}">
    <?php endif ?>

    @include('layouts.partials.navbar')
@endsection

@section('content')
    <div style="min-height: 80vh">
    <?php if(isset($electionDate)): ?>

    <div class="card m-0">
            <div class="card-body">
                <center><h1>Elections will take place in <span id="days"></span> Days <span id="hours"></span> Hours <span id="minutes"></span> Minutes <span id="secondes"></span> Secondes</h1></center>
            </div>
        </div>
    <?php endif ?>

        <div class="container-fluid">
            <div class="row p-t-1">
                <div class="col-12">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="max-height: 400px">
                        <ol class="carousel-indicators">
                          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner" style="max-height: 400px">
                          <div class="carousel-item active">
                            <img src="{{ asset('images/bg-title-01.jpg') }}" class="d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="{{ asset('images/bg-title-02.jpg') }}" class="d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="{{ asset('images/work3.jpg') }}" class="d-block w-100" alt="...">
                          </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                </div>
                <div class="col-12 p-t-10">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <center><h3>Electors ({{ $votants }})</h3></center>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint, aliquam, maxime nemo tempore providen
                                    t corrupti quis suscipit praesen
                                    tium, atque sit fugit vitae alias excepturi ipsum sapiente incidunt cupiditate velit ullam?
                                    <div class="d-lg-block text-center">
                                        <a href="{{ route('votantLogin') }}" class="btn btn-success">Login as elecotor</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <center><h3>Candidates ({{ $candidats }})</h3></center>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint, aliquam, maxime nemo tempore providen
                                    t corrupti quis suscipit praesen
                                    tium, atque sit fugit vitae alias excepturi ipsum sapiente incidunt cupiditate velit ullam?
                                    <div class="d-lg-block text-center">
                                        <a href="{{ route('candidatLogin') }}" class="btn btn-success">Login as candidate</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        jQuery(function($)
    {
        //var launch = new Date(2020, 10, 7, 14, 00, 00);
        //var l = document.getElementsByName('launchDate')[0].value;
        //alert(l);
        //console.log(l);
        var launch = new Date(document.getElementsByName('launchDate')[0].value);
        var day = document.getElementById('days');
        var hour = document.getElementById('hours');
        var minute = document.getElementById('minutes');
        var seconde = document.getElementById('secondes');

        /*$.get('server.php', function(data) {
            //var serverDate = new Date(data);
            //console.log(serverDate);

            console.log(data);
          });*/

          /*$.ajax({
            url: "server.php",
            type: "get",
            async: "false",
            success: function(data)
            {
                data = JSON.parse(data);

                launch = new Date(data[1]);
                setDate(launch);
            }
        });*/




        setDate();
        function setDate()
        {
            var now = new Date();

            s = (launch.getTime() - now.getTime())/1000;

            //console.log(now, launch, s);

            var d = Math.floor(s/86400);
            //console.log("==============================================        " + d);
            day.innerHTML = d;
            s -= d*86400;



            var h = Math.floor(s/3600);
            //console.log("==============================================        " + h);
            hour.innerHTML = h;
            s -= h*3600;

            var m = Math.floor(s/60);
            //console.log("==============================================        " + m);
            minute.innerHTML = m;
            s -= m*60;

            s = Math.floor(s);
            //console.log("==============================================        " + s);
            seconde.innerHTML = s;

            setTimeout(setDate, 1000);
        }

    });
    </script>
@endsection

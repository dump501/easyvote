@extends('layouts.candidat')
@section('content')
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row m-t-30">
                <div class="col-md-12">
                    <h3>Name:<br> {{ $party->name }}</h3>
                </div><br><br><br>
                <div class="col-md-12">
                    <h3>Description:<br> </h3> <h5>{{ $party->description }}</h5>
                </div><br><br><br>
                <div class="col-md-12">
                    <h3>Creation Date:<br> {{ $party->created_at }}</h3>
                </div><br><br><br>
                <div class="col-md-12">
                    <h3>Envolved election:<br> {{ $party->election->name }}  Taking place the {{ $party->election->done_at }}</h3>
                </div>
                <div class="col-md-12">
                    Contact the admin to change party informations
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection

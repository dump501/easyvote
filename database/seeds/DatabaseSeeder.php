<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('admins')->insert([
            'name' => 'Albin Tsafack',
            'email' => 'admin@admin.com',
            'password' => Hash::make('albin123'),
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('candidats')->insert([
            'name' => 'Fritz Djiogo',
            'email' => 'candidat@easyvote.com',
            'password' => Hash::make('albin123'),
            'party_id' => 1,
        ]);
        DB::table('votants')->insert([
            'name' => 'Joël Tsafack',
            'email' => 'votant@easyvote.com',
            'password' => Hash::make('albin123'),
        ]);
        DB::table('elections')->insert([
            'name' => 'departemental elections',
            'description' => 'description de desctiop tion qsfsqf sqflsjqhf qsfd ol',
            'done_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('elections')->insert([
            'name' => 'school elections',
            'description' => 'description de desctiop tion qsfsqf sqflsjqhf qsfd ol',
            'done_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('parties')->insert([
            'name' => 'yellow party',
            'election_id' => 1,
        ]);
        DB::table('parties')->insert([
            'name' => 'pink party',
            'election_id' => 1,
        ]);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->id();
            $table->integer('election_id');
            $table->integer('party_id');
            $table->integer('votant_id');
            $table->timestamps();

            $table->foreign('election_id')->references('id')->on('elections');
            $table->foreign('party_id')->references('id')->on('parties');
            $table->foreign('votant_id')->references('id')->on('votants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');
    }
}

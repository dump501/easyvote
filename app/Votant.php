<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Votant extends Authenticatable
{
    //
    use Notifiable;

    protected $guard = 'votant';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'password',
    ];

    public function vote()
    {
        return $this->hasMany('App\Vote');
    }
}

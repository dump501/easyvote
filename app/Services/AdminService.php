<?php

namespace App\Services;


class AdminService
{
    public function getInfos(Request $request)
    {
        return $request->session()->get('currentUser');
    }
}
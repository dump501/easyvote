<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Party extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'election_id',
    ];

    public function candidat()
    {
        return $this->belongsTo('App\Candidat');
    }

    public function vote()
    {
        return $this->hasMany('App\Vote');
    }

    public function election(){
        return $this->belongsTo('App\Election');
    }

}

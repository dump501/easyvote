<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'candidat_id', 'votant_id', 'election_id',
    ];

    public function election()
    {
        return $this->belongsTo('App\Election');
    }

    public function votant()
    {
        return $this->belongsTo('App\Votant');
    }

    public function party()
    {
        return $this->belongsTo('App\Party');
    }
}

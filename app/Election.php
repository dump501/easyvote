<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Election extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function vote()
    {
        return $this->hasMany('App\Vote');
    }

}

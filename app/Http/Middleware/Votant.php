<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Votant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guard('votant')->check())
        {
            return redirect('/login/votant');
        }
        return $next($request);
    }
}

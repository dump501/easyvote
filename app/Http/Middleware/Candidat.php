<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Candidat
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guard('candidat')->check())
        {
            return redirect('/login/candidat');
        }
        return $next($request);
    }
}

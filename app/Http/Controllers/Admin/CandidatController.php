<?php

namespace App\Http\Controllers\Admin;

use App\Election;
use App\Http\Controllers\Controller;
use App\Jobs\ElectionAlert;
use App\Party;
use Illuminate\Http\Request;

use App\Candidat;
use Illuminate\Support\Facades\Hash;

class CandidatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $candidats = Candidat::all();
        return view('admin.candidat.index', compact('candidats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $parties = Party::all();
        return view('admin.candidat.create', compact('parties'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd(date_create($request->done_at));
        $request->validate([
            'name' => 'required|min:3',
            'email' =>'required|min:3',
            'phone' => 'required',
            'password' => 'required',
            'party_id' => 'required'
        ]);


        $candidat = new Candidat();
        $candidat->name = $request->name;
        $candidat->email = $request->email;
        $candidat->phone = $request->phone;
        $candidat->party_id = $request->party_id;
        $candidat->password = Hash::make($request->password);
        $candidat->save();

        return redirect()->route('admin.candidats.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Candidat::destroy($id);
        return redirect(route('admin.candidats.index'));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Candidat;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Votant;
use Illuminate\Support\Facades\Hash;

class VotantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $votants = Votant::all();
        return view('admin.votant.index', compact('votants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view("admin.votant.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd(date_create($request->done_at));
        $request->validate([
            'name' => 'required|min:3',
            'email' =>'required|min:3',
            'phone' => 'required',
            'password' => 'required',
        ]);


        $votant = new Votant();
        $votant->name = $request->name;
        $votant->email = $request->email;
        $votant->phone = $request->phone;
        $votant->password = Hash::make($request->password);
        $votant->save();

        return redirect()->route('admin.votants.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Votant::destroy($id);
        return redirect(route('admin.votants.index'));
    }
}

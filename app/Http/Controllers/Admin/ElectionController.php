<?php

namespace App\Http\Controllers\Admin;

use App\Election;
use App\Jobs\ElectionAlert;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ElectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $elections = Election::all();
        return view('admin.election.index', compact('elections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.election.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd(date_create($request->done_at));
        $request->validate([
            'name' => 'required|min:3',
            'description' =>'required|min:3',
            'done_at' => 'required',
        ]);

        $mysqltime = date_create($request->done_at);
        //$x = Carbon::today();
        //Carbon::c
        //dd();
        $election = new Election();
        $election->name = $request->name;
        $election->description = $request->description;
        $election->done_at = $mysqltime;
        $election->save();

        dispatch(new ElectionAlert($election));

        return redirect()->route('admin.elections.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Election::destroy($id);
        return redirect(route('admin.parties.index'));
    }
}

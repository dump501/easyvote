<?php

namespace App\Http\Controllers\Admin;

use App\Election;
use App\Http\Controllers\Controller;
use App\Vote;
use App\Party;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $election=Election::all();
        // $x = Carbon::today();
        // dd($x->toDateTime());
        // die();
        $voteArray = [];
        $winner = "";
        $election = null;


        $customElections = Election::all();
        foreach ($customElections as $elec) {
            $elecDate = $elec->done_at;

            $electDates = explode(" ", $elecDate);

            if($electDates[0] == explode(" ", date(now()))[0]){
                $election = $elec;
                break;
            }
        }
        // $election = Election::whereDate('done_at','=', Carbon::today()->toDateTime())->get();
        if($election == null){
            return view('admin.votes.index', compact('voteArray', 'winner'));
        }
        else{
            // $votes = Vote::where('election_id', $election[0]->id)->get();
            $height = 0;

            $parties = Party::where(['election_id'=>$election->id])->get();
            foreach ($parties as $party) {
                $votes = Vote::where(['election_id'=>$election->id, 'party_id'=>$party->id])->get();
                $voteArrayItem = [
                    "party"=>$party->name,
                    "vote"=>count($votes)
                ];
                if(count($votes) > $height){
                    $winner = $party->name;
                }
                $voteArray[] = $voteArrayItem;
                // dd($voteArray);
            }

        }

        return view('admin.votes.index', compact('voteArray', 'winner'));
        // dd(count($election));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $elections = Election::all();
        return view('admin.votes.create', compact('elections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $electionDetail = array();
        $election = Election::find($id)->get()[0];
        $parties = Party::where(['election_id'=> $id])->get();
        $height = 0;
        $winner = "";
        foreach ($parties as $party) {
            $votes = Vote::where(['election_id'=>$id, 'party_id'=>$party->id])->get();

            $item = array();
            $item['party'] = $party->name;
            $item['votes'] = count($votes);

            $electionDetail[] = $item;

            if(count($votes)>=$height){
                $height = count($votes);
                $winner = $party->name;
            }

        }

        return view('admin.votes.show', compact('electionDetail', 'winner', 'election'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

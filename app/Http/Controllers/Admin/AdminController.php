<?php

namespace App\Http\Controllers\Admin;

use App\Candidat;
use App\Election;
use App\Http\Controllers\Controller;
use App\Votant;
use Illuminate\Http\Request;
use Auth;
use App\Admin;


class AdminController extends Controller
{

    public function home()
    {
        //dd(Auth::id());
        $elections = count(Election::all());
        $votants = count(Votant::all());
        $candidats = count(Candidat::all());
        return view('admin.home', compact('elections', 'votants', 'candidats'));
    }
}

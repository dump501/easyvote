<?php

namespace App\Http\Controllers\Candidat;

use App\Candidat;
use App\Election;
use App\Votant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class CandidatController extends Controller
{
    public function home()
    {

        $elections = count(Election::all());
        $votants = count(Votant::all());
        $candidats = count(Candidat::all());
        return view('candidat.home', compact('elections', 'votants', 'candidats'));
    }
}

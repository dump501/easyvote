<?php

namespace App\Http\Controllers\Candidat;

use App\Election;
use App\Http\Controllers\Controller;
use App\Party;
use App\Vote;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class VoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $voteArray = [];
        $winner = "";
        $election = null;


        $customElections = Election::all();
        foreach ($customElections as $elec) {
            $elecDate = $elec->done_at;

            $electDates = explode(" ", $elecDate);

            if($electDates[0] == explode(" ", date(now()))[0]){
                $election = $elec;
                break;
            }
        }

        // $election = Election::whereDate('done_at','=', date(now()))->get();
        // dd(DB::select("select * from elections where done_at=?", [date(now())]));
        // $election = DB::select("select * from elections where done_at=?", [date(now())]);
        // dd($election);
        if($election == null){
            dd('h');
            return view('candidat.vote.index', compact('voteArray', 'winner', 'election'));
        }
        else{

            // $votes = Vote::where('election_id', $election[0]->id)->get();
            $height = 0;

            $parties = Party::where(['election_id'=>$election->id])->get();
            foreach ($parties as $party) {
                $votes = Vote::where(['election_id'=>$election->id, 'party_id'=>$party->id])->get();
                $voteArrayItem = [
                    "party"=>$party->name,
                    "vote"=>count($votes)
                ];
                if(count($votes) > $height){
                    $winner = $party->name;
                }
                $voteArray[] = $voteArrayItem;
                // dd($voteArray);
            }

            // dd($voteArray);

        }

        return view('candidat.vote.index', compact('voteArray', 'winner', 'election'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

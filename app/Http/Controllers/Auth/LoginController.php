<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

use App\Admin;
use App\Candidat;
use App\Votant;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('logout');
        $this->middleware('guest:candidat')->except('logout');
        $this->middleware('guest:votant')->except('logout');
    }

    /**
     * admin login
     */
    public function showAdminLoginForm()
    {
        return view('auth.login', ['url' => 'admin']);
    }

    public function adminLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'email',
            'phone' => 'integer',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            //memoriser les identifiants en session
            $req = Admin::where('email',$request->email)->get();
            $currentUser = array(
                "id" => $req[0]->id,
                "name" => $req[0]->name,
                "role" => "admin"
            );
            
            $request->session()->put('currentUser',$currentUser);

            return redirect()->intended(route('admin.home'));
        }
        return back()->withInput($request->only('email', 'remember'));
    }

    /**
     * candidat login
     */
    public function showCandidatLoginForm()
    {
        return view('auth.login', ['url' => 'candidat']);
    }

    public function candidatLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'email',
            'phone' => 'integer',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('candidat')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            
            //memoriser les identifiants en session
            $req = Candidat::where('email',$request->email)->get();
            $currentUser = array(
                "id" => $req[0]->id,
                "name" => $req[0]->name,
                "role" => "candidat"
            );
            
            $request->session()->put('currentUser',$currentUser);

            return redirect()->intended(route('candidat.home'));
        }
        return back()->withInput($request->only('email', 'remember'));
    }

    /**
     * votant login
     */
    public function showVotantLoginForm()
    {
        return view('auth.login', ['url' => 'votant']);
    }

    public function votantLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'email',
            'phone' => 'integer',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('votant')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            
            //memoriser les identifiants en session
            $req = Votant::where('email',$request->email)->get();
            $currentUser = array(
                "id" => $req[0]->id,
                "name" => $req[0]->name,
                "role" => "votant"
            );
            
            $request->session()->put('currentUser',$currentUser);

            return redirect()->intended(route('votant.home'));
        }
        return back()->withInput($request->only('email', 'remember'));
    }

    /*
    public function username()
    {
        return 'phone';
    }*/
}

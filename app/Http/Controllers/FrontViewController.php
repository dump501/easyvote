<?php

namespace App\Http\Controllers;

use App\Election;
use App\Candidat;
use App\Votant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class FrontViewController extends Controller
{
    //

    public function welcome()
    {
        $candidats = Candidat::all();
        $candidats = count($candidats);
        $votants = Votant::all();
        $votants = count($votants);
        $election = Election::whereDate('done_at','=', Carbon::today()->toDateTime())->get();

        $serverDate = new \DateTime('NOW');
        $electionDate = array();
        if(isset($election[0])){

            $finalDate = new \DateTime($election[0]->done_at);

            $electionDate[0] = $serverDate->format(DATE_ATOM);
            $electionDate[1] = $finalDate->format(DATE_ATOM);

            return view('welcome', compact('electionDate', 'candidats', 'votants'));
        }
        else{

            return view('welcome', compact('electionDate', 'candidats', 'votants'));
        }

    }
}

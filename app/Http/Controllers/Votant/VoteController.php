<?php

namespace App\Http\Controllers\Votant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Vote;
use App\Election;
use App\Party;
use Illuminate\Support\Facades\Auth;

class VoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $elections = Election::all();
        //dd(Auth::guard('votant')->user()->id);

        $voteArray = [];
        $winner = "";
        $election = null;


        $customElections = Election::all();
        foreach ($customElections as $elec) {
            $elecDate = $elec->done_at;

            $electDates = explode(" ", $elecDate);

            if($electDates[0] == explode(" ", date(now()))[0]){
                $election = $elec;
                break;
            }
        }
        //dd(Carbon::today()->toDate());
        // $election = Election::whereDate('done_at','=', Carbon::today()->toDateTime())->get();
        //dd($election);
        if($election == null){
            return view('votant.vote.index', compact('election'));
        }
        else{
            $vote = Vote::where(['election_id' => $election->id, 'votant_id' => Auth::guard('votant')->user()->id])->get();
            //dd($election);
            // dd(isset($vote[0]));
            $parties = Party::where('election_id', $election->id)->get();

            $height = 0;
            foreach ($parties as $party) {
                $votes = Vote::where(['election_id'=>$election->id, 'party_id'=>$party->id])->get();

                $voteArrayItem = [
                    "party"=>$party->name,
                    "vote"=>count($votes)
                ];
                if(count($votes) > $height){
                    $winner = $party->name;
                }
                $voteArray[] = $voteArrayItem;
                // dd($voteArray);
            }


            return view('votant.vote.index', compact('votes','parties', 'winner', 'voteArray', 'election', 'vote'));
        }
    }

    /**
     * find the parti enrolled in an election
     */
    public function findElectionParty(Request $request)
    {
        $election_id = $request->election_id;

        $parties = Party::where('election_id',$election_id)
                            ->get();



        return response()->json([
            'parties' => $parties
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'party' => 'required|integer',
        ]);
        $election = null;


        $customElections = Election::all();
        foreach ($customElections as $elec) {
            $elecDate = $elec->done_at;

            $electDates = explode(" ", $elecDate);

            if($electDates[0] == explode(" ", date(now()))[0]){
                $election = $elec;
                break;
            }
        }

        $vote = new Vote();
        $vote->election_id = $election->id;
        $vote->party_id = $request->party;
        $vote->votant_id = $request->session()->get('currentUser')['id'];

        $vote->save();

        return redirect(route('votant.home'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Jobs;

use App\Election;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ElectionAlert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $election;
    public function __construct(Election $election)
    {
        $this->election = $election;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $MessageBird = new \MessageBird\Client('Zm7l1mVoPqC37n6O9NTln25Dw');
        $Message = new \MessageBird\Objects\Message();
        $Message->originator = 'EasyVote';
        $Message->recipients = array(237681617121);
        $Message->body = "Hello!, it is the easyvote platform The election ". $this->election->name ." has been created an will take place the" . $this->election->done_at . " Do not foget to vote thanks you :)";

        $MessageBird->messages->create($Message);
    }
}
